# own-server

Example data receiver for Rastri packets data delivery.

# Usage

## CLIENT_KEY

Export a `CLIENT_KEY` environment variable that is going
to be used as your server authorization token.

Every request must declare an `Authorization` header:

> Authorization: token <client-key>

## run.sh

Run `run.sh` script.

# Usage with Heroku

    cd own-client
    heroku apps:create <your-new-app-name>
    heroku config:set CLIENT_KEY=<your-key>
    git push heroku master

# Get your key

**Not available, yet.**
