from os import environ

import bottle
from lazywf import TheLaziestWebFrameworkEVER
from lazywf.rest import RestOperationsManager


CLIENT_KEY = environ['CLIENT_KEY']


class MyRestManager(RestOperationsManager):
    def create(self, *args, **kwargs):
        headers = bottle.request.headers
        token_string = headers.get('authorization', None)

        if token_string is None:
            return bottle.HTTPResponse(status=401, body='You are not authorized')

        token_type, token = token_string.split(' ')
        if token_type != 'token':
            return bottle.HTTPResponse(
                status=401, body=f'Invalid authorization type: {token_type}'
            )

        if token != CLIENT_KEY:
            return bottle.HTTPResponse(
                status=401, body=f'Invalid token'
            )

        return super().create(*args, **kwargs)


class Lazy(TheLaziestWebFrameworkEVER):
    def load_rest_manager(self):
        self.rest_manager = MyRestManager(self)


Lazy().run()
